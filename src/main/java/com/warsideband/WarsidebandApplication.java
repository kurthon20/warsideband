package com.warsideband;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WarsidebandApplication {

	public static void main(String[] args) {
		SpringApplication.run(WarsidebandApplication.class, args);
	}

}
